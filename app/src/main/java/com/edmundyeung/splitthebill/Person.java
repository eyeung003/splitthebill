package com.edmundyeung.splitthebill;

import java.util.UUID;

/**
 * Person.
 * Represents a unique person, with an identifier
 */
public class Person {

    public UUID id;

    public String name;

    public Person(String name){
        this.name = name;
        this.id = UUID.randomUUID();
    }

}
