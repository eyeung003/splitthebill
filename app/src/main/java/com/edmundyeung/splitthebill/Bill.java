package com.edmundyeung.splitthebill;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

/**
 * Bill object to store bill items and calculate how to split the bill amongst the list of people
 */
public class Bill {

    //TODO make these configurable via settings
    public final SplitMethod DEFAULT_SPLIT_METHOD = SplitMethod.Evenly;
    public final double DEFAULT_TAX = 0; //Tax is included in price
    public final double DEFAULT_SERVICE = 0; //No tip
    public final Boolean DEFAULT_TIP_BEFORE_TAX = true; //tip on amount before tax

    public enum SplitMethod {
        Evenly, ByItem
    }

    public UUID id;

    private Hashtable<UUID, Person> people;
    private List<BillItem> billItems;

    private double tax = DEFAULT_TAX; //%
    private double service = DEFAULT_SERVICE; //%

    private Boolean serviceBeforeTax = DEFAULT_TIP_BEFORE_TAX;

    private SplitMethod splitMethod = DEFAULT_SPLIT_METHOD;

    public Bill(){
        people = new Hashtable<>();
        billItems = new ArrayList<>();
        this.id = UUID.randomUUID();
    }

    public Hashtable<UUID, Person> getPeople(){
        return people;
    }

    public void addPerson(Person p) {
        if(!people.containsKey(p.id)) people.put(p.id, p);
    }

    public List<BillItem> getBillItems() {
        return billItems;
    }

    public void addBillItem(BillItem billItem) {
        billItems.add(billItem);
    }

    public void setTaxPercentage(double taxPercentage) {
        tax = taxPercentage;
    }

    public void setServicePercentage(double servicePercentage) {
        service = servicePercentage;
    }

    public void setServiceBeforeTax(boolean serviceBeforeTax) {
        this.serviceBeforeTax = serviceBeforeTax;
    }

    public void setSplitMethod(SplitMethod splitMethod) {
        this.splitMethod = splitMethod;
    }

    /**
     * Add all bill items by their amount
     */
    public BigDecimal getNetTotal() {
        BigDecimal sum = new BigDecimal(0);
        for (int i = 0; i < billItems.size(); i++) {
            sum = sum.add(billItems.get(i).getAmount(false));
        }
        return sum.setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Calculate the amount owing for the person
      */
    public BigDecimal getAmountOwing(Person person) {
        if(people.size() <= 0)
            throw new ArithmeticException("No people added to the bill"); //TODO create a custom exception
        if(!people.containsKey(person.id))
            throw new ArrayIndexOutOfBoundsException("Person is not added to the bill"); //TODO create a custom exception

        BigDecimal taxPercent = new BigDecimal(tax).setScale(2, RoundingMode.HALF_UP);
        BigDecimal servicePercent = new BigDecimal(service).setScale(2, RoundingMode.HALF_UP);

        switch (splitMethod){
            case Evenly:

                BigDecimal netAmount = getNetTotal();

                BigDecimal totalAmountOwing = addTaxAndService(taxPercent, servicePercent, netAmount);

                return divideByAllPeople(totalAmountOwing);

            case ByItem:
                BigDecimal netAmountForPerson = getNetAmountForPerson(person);

                BigDecimal totalAmountForPerson = addTaxAndService(taxPercent, servicePercent, netAmountForPerson);

                return totalAmountForPerson.setScale(2, RoundingMode.HALF_UP);

            default:
                throw new UnsupportedOperationException("Split Method not recognised");
        }
    }

    private BigDecimal addTaxAndService(BigDecimal taxPercent, BigDecimal servicePercent, BigDecimal netAmount) {

        BigDecimal taxAmount = netAmount.multiply(taxPercent);
        BigDecimal totalAmountOwing = netAmount.add(taxAmount);

        //determine whether to add service charge before/after additional tax has been included
        BigDecimal serviceAmount;
        if(serviceBeforeTax) serviceAmount = netAmount.multiply(servicePercent);
        else serviceAmount = totalAmountOwing.multiply(servicePercent);

        totalAmountOwing = totalAmountOwing.add(serviceAmount);
        return totalAmountOwing;
    }

    /**
     * Loop through bill items to check for the owner, sum the amounts for all items that the person owns
     */
    private BigDecimal getNetAmountForPerson(Person person) {
        BigDecimal amountPayableForPerson = new BigDecimal(0);

        for (int i = 0; i < billItems.size(); i++) {

            BillItem billItem = billItems.get(i);

            //check if there are any owners
            if(billItem.getOwners().size() == 0){
                //if none, shared between all people
                amountPayableForPerson = amountPayableForPerson.add(divideByAllPeople(billItem.getAmount(false)));
            }
            //otherwise check if this person is the owner of this bill item
            else if(billItem.getOwners().containsKey(person.id)){
                //shared between all the owners of this bill item
                BigDecimal sharedAmount = billItem.getAmount(false).divide(new BigDecimal(billItem.getOwners().size()), 2, RoundingMode.HALF_UP);
                amountPayableForPerson = amountPayableForPerson.add(sharedAmount);
            }
        }
        return amountPayableForPerson;
    }

    private BigDecimal divideByAllPeople(BigDecimal amount){
        return amount.divide(new BigDecimal(people.size()), 2, RoundingMode.HALF_UP);
    }
}
