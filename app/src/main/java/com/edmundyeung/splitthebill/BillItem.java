package com.edmundyeung.splitthebill;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

/**
 * Represents a unique bill item to add to the bill e.g. food and drinks
 */
public class BillItem {

    public UUID id;

    public String name;

    private BigDecimal amount;

    //Person who will pay for the item
    private Hashtable<UUID, Person> owners;

    public BillItem(String name, BigDecimal amount){
        this.name = name;
        this.amount = amount;
        this.id = UUID.randomUUID();
        this.owners = new Hashtable<>();
    }

    public BigDecimal getAmount(){
        return getAmount(true);
    }

    public BigDecimal getAmount(Boolean rounded){
        if(rounded) return amount.setScale(2, RoundingMode.HALF_UP);
        else return amount;
    }

    public void addOwner(Person owner) {
        if(!owners.containsKey(owner.id)) owners.put(owner.id, owner);
    }

    public void removeOwner(Person owner) {
        if(owners.containsKey(owner.id)) owners.remove(owner.id);
    }

    public Hashtable<UUID, Person> getOwners() {
        return owners;
    }
}
