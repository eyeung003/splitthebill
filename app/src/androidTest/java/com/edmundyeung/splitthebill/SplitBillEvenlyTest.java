package com.edmundyeung.splitthebill;

import android.test.AndroidTestCase;

import java.math.BigDecimal;

/**
 *
 */
public class SplitBillEvenlyTest extends AndroidTestCase {

    /**
     * The most basic way to split the bill is to divide the total evenly amongst all people
     * */
    public void testSplitBillEvenly(){

        Bill testBill = new Bill();

        testBill.addBillItem(new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00)));
        testBill.addBillItem(new BillItem("Small Roast Chicken Leg", new BigDecimal(50.00)));
        testBill.addBillItem(new BillItem("Mead", new BigDecimal(20.50)));
        testBill.addBillItem(new BillItem("Mead", new BigDecimal(20.50)));

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        assertEquals("95.50", amountDueForPerson1.toString());
        assertEquals("95.50", amountDueForPerson2.toString());
    }

    public void testSplitBillEvenlyBetweenThree(){

        Bill testBill = new Bill();

        testBill.addBillItem(new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00)));

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        Person person3 = new Person("Sansa Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);
        testBill.addPerson(person3);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);
        BigDecimal amountDueForPerson3 = testBill.getAmountOwing(person3);

        assertEquals("33.33", amountDueForPerson1.toString());
        assertEquals("33.33", amountDueForPerson2.toString());
        assertEquals("33.33", amountDueForPerson3.toString());

        //the problem with this kind of division is that there will be $0.01 left outstanding on the bill
    }

    public void testSplitBillWithAdditionalTax(){

        Bill testBill = new Bill();

        testBill.addBillItem(new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00)));

        testBill.setTaxPercentage(0.15);

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        assertEquals("57.50", amountDueForPerson1.toString());
        assertEquals("57.50", amountDueForPerson2.toString());
    }

    public void testSplitBillWithService(){

        Bill testBill = new Bill();

        testBill.addBillItem(new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00)));

        testBill.setServicePercentage(0.10);

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        assertEquals("55.00", amountDueForPerson1.toString());
        assertEquals("55.00", amountDueForPerson2.toString());
    }

    public void testSplitBillWithAdditionalTaxAndServiceBeforeTax(){

        Bill testBill = new Bill();

        testBill.addBillItem(new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00)));

        testBill.setTaxPercentage(0.15);
        testBill.setServicePercentage(0.10);

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        //100 + (100*0.15) + (100*0.1) = 125
        //125 / 2 = 62.5
        assertEquals("62.50", amountDueForPerson1.toString());
        assertEquals("62.50", amountDueForPerson2.toString());
    }

    public void testSplitBillWithAdditionalTaxAndServiceAfterTax(){

        Bill testBill = new Bill();

        testBill.addBillItem(new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00)));

        testBill.setTaxPercentage(0.15);
        testBill.setServicePercentage(0.10);

        testBill.setServiceBeforeTax(false);//add service charge after additional tax has been included

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        //100 + (100*.15) = 115
        //115 + (115*.1) = 126.5
        //126.5 / 2 = 63.25
        assertEquals("63.25", amountDueForPerson1.toString());
        assertEquals("63.25", amountDueForPerson2.toString());
    }
}
