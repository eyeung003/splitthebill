package com.edmundyeung.splitthebill;

import android.test.AndroidTestCase;

import junit.framework.Assert;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

/**
 * Facts about the Bill class
 */
public class BillTest extends AndroidTestCase {

    public void testCreateBill() {
        Bill testBill = new Bill();

        assertEquals(0, testBill.getPeople().size());
        assertEquals(0, testBill.getBillItems().size());
    }

    public void testAddPeopleToBill() {
        Bill testBill = new Bill();

        Person p = new Person("Ned Stark");
        testBill.addPerson(p);

        Hashtable<UUID, Person> people = testBill.getPeople();

        assertEquals(1, people.size());
        assertEquals("Ned Stark", people.get(p.id).name);
    }

    public void testAddBillItemsToBill() {
        Bill testBill = new Bill();

        testBill.addBillItem(new BillItem("Roast Chicken Leg", new BigDecimal(100.00)));

        assertEquals(1, testBill.getBillItems().size());
        assertEquals("Roast Chicken Leg", testBill.getBillItems().get(0).name);
        assertEquals("100.00", testBill.getBillItems().get(0).getAmount().toString());
    }

    public void testBillTotal() {
        Bill testBill = new Bill();

        testBill.addBillItem(new BillItem("Roast Chicken Leg", new BigDecimal(100.00)));
        assertEquals(0, testBill.getNetTotal().compareTo(new BigDecimal(100.00)));

        testBill.addBillItem(new BillItem("Mead", new BigDecimal(20.50)));
        assertEquals("120.50", testBill.getNetTotal().toString());
    }

    /**
     * Exceptional cases:
     * No people or no bill items
     */
    public void testSplitBillWithNoPeople(){
        Bill testBill = new Bill();

        testBill.addBillItem(new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00)));

        Person person1 = new Person("Ned Stark");

        try {
            testBill.getAmountOwing(person1);

            Assert.fail("Should have thrown ArithmeticException");
        }
        catch(ArithmeticException ex){
            //success
        }
        catch(Exception ex){
            Assert.fail("Should have thrown ArithmeticException");
        }
    }

    public void testSplitBillWithPersonNotAdded(){

        Bill testBill = new Bill();

        testBill.addBillItem(new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00)));

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        Person notAddedPerson = new Person("Sansa Stark");

        try {
            testBill.getAmountOwing(notAddedPerson);

            Assert.fail("Should have thrown ArrayIndexOutOfBoundsException");
        }
        catch(ArrayIndexOutOfBoundsException ex){
            //success
        }
        catch(Exception ex){
            Assert.fail("Should have thrown ArrayIndexOutOfBoundsException");
        }
    }

    public void testSplitBillWithNoBillItems(){
        Bill testBill = new Bill();

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        assertEquals("0.00", amountDueForPerson1.toString());
        assertEquals("0.00", amountDueForPerson2.toString());
    }
}
