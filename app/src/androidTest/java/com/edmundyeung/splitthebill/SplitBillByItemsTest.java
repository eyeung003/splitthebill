package com.edmundyeung.splitthebill;

import android.test.AndroidTestCase;

import java.math.BigDecimal;

/**
 * Created by Edmund on 21/03/2015.
 */
public class SplitBillByItemsTest extends AndroidTestCase {

    /**
     * Split the bill by getting all people to pay for what they ordered
     * */
    public void testSplitTheBillByItem(){

        Bill testBill = new Bill();

        BillItem item1 = new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00));
        BillItem item2 = new BillItem("Small Roast Chicken Leg", new BigDecimal(50.00));
        BillItem item3 = new BillItem("Mead", new BigDecimal(20.50));
        BillItem item4 = new BillItem("Mead", new BigDecimal(20.50));

        testBill.addBillItem(item1);
        testBill.addBillItem(item2);
        testBill.addBillItem(item3);
        testBill.addBillItem(item4);

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        //overide default split method
        testBill.setSplitMethod(Bill.SplitMethod.ByItem);

        //associate the bill items with the people
        item1.addOwner(person1);
        item2.addOwner(person2);
        item3.addOwner(person1);
        item4.addOwner(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        assertEquals("120.50", amountDueForPerson1.toString());
        assertEquals("70.50", amountDueForPerson2.toString());
    }

    public void testSplitTheBillByItemWithAdditionalTax(){

        Bill testBill = new Bill();

        BillItem item1 = new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00));
        BillItem item2 = new BillItem("Small Roast Chicken Leg", new BigDecimal(50.00));

        testBill.setTaxPercentage(0.15);

        testBill.addBillItem(item1);
        testBill.addBillItem(item2);

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        //overide default split method
        testBill.setSplitMethod(Bill.SplitMethod.ByItem);

        //associate the bill items with the people
        item1.addOwner(person1);
        item2.addOwner(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        assertEquals("115.00", amountDueForPerson1.toString());
        assertEquals("57.50", amountDueForPerson2.toString());
    }

    public void testSplitTheBillByItemWithService(){

        Bill testBill = new Bill();

        BillItem item1 = new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00));
        BillItem item2 = new BillItem("Small Roast Chicken Leg", new BigDecimal(50.00));

        testBill.setServicePercentage(0.10);

        testBill.addBillItem(item1);
        testBill.addBillItem(item2);

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        //overide default split method
        testBill.setSplitMethod(Bill.SplitMethod.ByItem);

        //associate the bill items with the people
        item1.addOwner(person1);
        item2.addOwner(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        assertEquals("110.00", amountDueForPerson1.toString());
        assertEquals("55.00", amountDueForPerson2.toString());
    }

    public void testSplitTheBillByItemWithAdditionalTaxAndServiceBeforeTax(){

        Bill testBill = new Bill();

        BillItem item1 = new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00));
        BillItem item2 = new BillItem("Small Roast Chicken Leg", new BigDecimal(50.00));

        testBill.setTaxPercentage(0.15);
        testBill.setServicePercentage(0.10);
        testBill.setServiceBeforeTax(true);

        testBill.addBillItem(item1);
        testBill.addBillItem(item2);

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        //overide default split method
        testBill.setSplitMethod(Bill.SplitMethod.ByItem);

        //associate the bill items with the people
        item1.addOwner(person1);
        item2.addOwner(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        assertEquals("125.00", amountDueForPerson1.toString());
        assertEquals("62.50", amountDueForPerson2.toString());
    }

    public void testSplitTheBillByItemWithAdditionalTaxAndServiceAfterTax(){

        Bill testBill = new Bill();

        BillItem item1 = new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00));
        BillItem item2 = new BillItem("Small Roast Chicken Leg", new BigDecimal(50.00));

        testBill.setTaxPercentage(0.15);
        testBill.setServicePercentage(0.10);
        testBill.setServiceBeforeTax(false);

        testBill.addBillItem(item1);
        testBill.addBillItem(item2);

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        //overide default split method
        testBill.setSplitMethod(Bill.SplitMethod.ByItem);

        //associate the bill items with the people
        item1.addOwner(person1);
        item2.addOwner(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        assertEquals("126.50", amountDueForPerson1.toString());
        assertEquals("63.25", amountDueForPerson2.toString());
    }

    /**
     * When an item has no owner, it will be split amongst all the people evenly
     */
    public void testSplitTheBillByItemSharedItemWithNoOwner(){

        Bill testBill = new Bill();

        BillItem item1 = new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00));

        testBill.addBillItem(item1);

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);

        //overide default split method
        testBill.setSplitMethod(Bill.SplitMethod.ByItem);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);

        assertEquals("50.00", amountDueForPerson1.toString());
        assertEquals("50.00", amountDueForPerson2.toString());
    }

    /**
     * When an item has multiple owners, it will be split amongst the owners evenly
     */
    public void testSplitTheBillByItemSharedItemWithMultipleOwners(){

        Bill testBill = new Bill();

        BillItem item1 = new BillItem("Large Roast Chicken Leg", new BigDecimal(100.00));

        testBill.addBillItem(item1);

        Person person1 = new Person("Ned Stark");
        Person person2 = new Person("Arya Stark");
        Person person3 = new Person("Sansa Stark");
        testBill.addPerson(person1);
        testBill.addPerson(person2);
        testBill.addPerson(person3);

        //overide default split method
        testBill.setSplitMethod(Bill.SplitMethod.ByItem);

        //associate the bill items with the people
        item1.addOwner(person1);
        item1.addOwner(person2);

        BigDecimal amountDueForPerson1 = testBill.getAmountOwing(person1);
        BigDecimal amountDueForPerson2 = testBill.getAmountOwing(person2);
        BigDecimal amountDueForPerson3 = testBill.getAmountOwing(person3);

        assertEquals("50.00", amountDueForPerson1.toString());
        assertEquals("50.00", amountDueForPerson2.toString());
        assertEquals("0.00", amountDueForPerson3.toString());
    }
}
